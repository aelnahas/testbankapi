# frozen_string_literal: true

class TestCaseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :description, :priority

  attribute :has_steps do |test_case|
    !test_case.test_steps.count.zero?
  end

  has_many :test_steps
end
