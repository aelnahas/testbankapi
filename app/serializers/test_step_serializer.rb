# frozen_string_literal: true

class TestStepSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :category, :content, :step_number
end
