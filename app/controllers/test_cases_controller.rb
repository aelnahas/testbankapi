# frozen_string_literal: true

class TestCasesController < ApplicationController
  def index
    render json: serialize(TestCase.all)
  end

  def show
    test_case = TestCase.find(params[:id])
    render json: serialize(test_case)
  end

  def destroy
    TestCase.find(params[:id]).destroy
    render status: :no_content
  end

  def create
    test_case = TestCase.create!(test_case_params)
    render json: serialize(test_case), status: :created
  end

  def update
    test_case = TestCase.find(params[:id]).update!(test_case_params)
    render json: test_case
  end

  private

    def test_case_params
      params.require(:test_case).permit(:name, :description, :priority)
    end

    def serialize(test_case)
      options = {}
      options[:includes] = [:test_steps]
      TestCaseSerializer.new(test_case, options).serializable_hash
    end
end
