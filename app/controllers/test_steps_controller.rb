# frozen_string_literal: true

class TestStepsController < ApplicationController
  def index
    render json: serialized(test_case.test_steps)
  end

  def show
    render json: serialized(test_step)
  end

  def destroy
    test_step.destroy
    render status: :no_content
  end

  def create
    test_case.test_steps.create!(step_params)
    render json: serialized(test_case.test_steps), status: :created
  end

  def update
    test_step.update!(step_params)
    render json: serialized(test_step), status: :ok
  end

  private

    def serialized(test_steps)
      TestStepSerializer.new(test_steps).serializable_hash
    end

    def test_case
      TestCase.find(params[:test_case_id])
    end

    def test_step
      step = test_case.test_steps.find_by(step_number: params[:id])
      raise ActiveRecord::RecordNotFound, step_not_found_msg unless step
      step
    end

    def step_not_found_msg
      "step number #{params[:id]} is not found for test case #{params[:test_case_id]}"
    end

    def step_params
      params.require(:test_step).permit(:step_number, :content, :category)
    end
end
