# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Error::ErrorHandler
  before_action :set_raven_context

  def page_not_found
    route = request.path
    render_error(:invalid_path, 400, "unknown path: #{route}")
  end


  private

    def set_raven_context
      Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    end
end
