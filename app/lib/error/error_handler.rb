# frozen_string_literal: true

module Error
  module ErrorHandler
    def self.included(clazz)
      clazz.class_eval do
        rescue_from ActiveRecord::RecordNotFound do |e|
          render_error(:record_not_found, 404, e.to_s)
        end

        rescue_from ActiveRecord::RecordInvalid do |e|
          render_error(:validation_errors, 422, e.record.errors)
        end

        rescue_from ActionController::ParameterMissing do |e|
          render_error(:bad_request, 400, e.to_s)
        end
      end
    end

    def error_payload(status, error, message)
      {
        status: status,
        error: error,
        message: message
      }
    end

    def render_error(error, status, message)
      render json: error_payload(status, error, message).as_json, status: status
    end
  end
end
