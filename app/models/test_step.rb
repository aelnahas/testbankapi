# frozen_string_literal: true

class TestStep < ApplicationRecord
  validates :step_number,
            presence: true,
            uniqueness: { scope: :test_case_id },
            numericality: { less_than: 100, only_integer: true }
  validates_length_of :content, in: 1..100

  belongs_to :test_case
  enum category: [:generic, :precondition, :assertion]
end
