# frozen_string_literal: true

class TestCase < ApplicationRecord
  validates :name, presence: true
  has_many :test_steps, dependent: :destroy

  enum priority: [ :sanity, :smoke, :regression]
end
