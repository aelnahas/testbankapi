# frozen_string_literal: true

require "rails_helper"

RSpec.describe TestCasesController, type: :controller do
  let(:test_cases) { build_list(:test_case, 3) }
  let!(:test_case) { create(:test_case) }

  describe "#index" do
    context "when no parameters are passed" do
      it "gets all test cases" do
        expect(TestCase).to receive(:all)
        get :index
      end
    end
  end


  describe "#show" do
    context "when valid id" do
      it "gets the test case" do
        expect(TestCase).to receive(:find).and_return(test_case)
        get :show, params: { id: test_case.id }
      end

      it "should have status 200" do
        get :show, params: { id: test_case.id }
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "#destroy" do
    it "removes the test case" do
      expect {
        delete :destroy, params: { id:  test_case }
      }.to change(TestCase, :count).by(-1)
    end
  end

  describe "#create" do
    context "with valid parameters" do
      it "creates a new test case" do
        expect {
          post :create, params: { test_case: test_case.as_json }
        }.to change(TestCase, :count).by(1)
      end
    end
  end
end
