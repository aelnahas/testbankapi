# frozen_string_literal: true

require "rails_helper"

RSpec.describe TestStepsController, type: :controller do
  let!(:test_case) { create(:test_case) }
  let!(:test_case_with_steps) { create(:test_case_with_steps) }
  let(:test_step_params) { attributes_for(:test_step) }

  describe "#create" do
    it "creates test steps" do
      expect {
        post :create, params: { test_case_id: test_case.id, test_step: test_step_params.as_json }
      }.to change(TestStep, :count).by(1)
    end
  end

  describe "#get" do
    let(:returned_step) { JSON.parse(response.body) }
    let(:expected_step) { test_case_with_steps.test_steps.first }

    it "gets the step with the correct step number" do
      get :show, params: {
        test_case_id: test_case_with_steps.id,
        id: expected_step.step_number
      }
      expect(returned_step["data"]["attributes"]["id"]).to eq(expected_step.id)
    end
  end
end
