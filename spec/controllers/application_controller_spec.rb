# frozen_string_literal: true

require "rails_helper"

describe ApplicationController, type: :controller do
  it { is_expected.to rescue_from(ActiveRecord::RecordNotFound) }
end
