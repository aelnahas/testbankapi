# frozen_string_literal: true

require "rails_helper"

describe "Error Handling", type: :request do
  context "when invalid route" do
    it "should respond with unknown path error" do
      get "/nowhere"
      expect(response).to have_http_status :bad_request
    end
  end
end
