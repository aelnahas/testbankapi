# frozen_string_literal: true

FactoryBot.define do
  factory :test_case do
    name { Faker::Name.name }
    description { Faker::Lorem.sentence }
    priority { :sanity }

    factory :test_case_with_steps do
      transient do
        test_step_counts { 5 }
      end

      after(:create) do |test_case, evaluator|
        create_list(:test_step, evaluator.test_step_counts, test_case: test_case)
      end
    end
  end
end
