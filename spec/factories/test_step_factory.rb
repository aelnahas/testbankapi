# frozen_string_literal: true

FactoryBot.define do
  factory :test_step do
    sequence(:step_number)
    content { Faker::Lorem.sentence }
    category { :precondition }
  end
end
