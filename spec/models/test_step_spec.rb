# frozen_string_literal: true

require "rails_helper"

RSpec.describe TestStep, type: :model do
  it { is_expected.to belong_to(:test_case) }
  it { is_expected.to validate_length_of(:content).is_at_least(1).is_at_most(100) }
  it { is_expected.to validate_presence_of(:step_number) }
  it { is_expected.to validate_numericality_of(:step_number).is_less_than(100).only_integer }
  it { is_expected.to validate_uniqueness_of(:step_number).scoped_to(:test_case_id) }
end
