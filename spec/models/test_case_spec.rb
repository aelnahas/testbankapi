# frozen_string_literal: true

require "rails_helper"

RSpec.describe TestCase, type: :model do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to have_many(:test_steps).dependent(:destroy) }
end
