FROM ruby:2.5.1-alpine3.7

RUN apk add --no-cache --update build-base \
    linux-headers \
    git \
    postgresql-dev \
    nodejs \
    tzdata

RUN mkdir -p /var/app
WORKDIR /var/app

COPY Gemfile* /var/app/

RUN bundle install

COPY . /var/app

EXPOSE 3000

CMD ["bundle", "exec", "rails", "s", "-p", "3000", "-b", "0.0.0.0"]