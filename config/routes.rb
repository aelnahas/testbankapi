# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :test_cases do
    resources :test_steps
  end

  match "*path", to: "application#page_not_found", via: :all
end
