# frozen_string_literal: true

class AddStepNumberToTestStep < ActiveRecord::Migration[5.2]
  def change
    add_column :test_steps, :step_number, :integer
  end
end
