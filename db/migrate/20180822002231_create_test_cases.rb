# frozen_string_literal: true

class CreateTestCases < ActiveRecord::Migration[5.2]
  def change
    create_table :test_cases do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
