# frozen_string_literal: true

class ChangeColumnNameTypeToCategory < ActiveRecord::Migration[5.2]
  def change
    rename_column :test_steps, :type, :category
  end
end
