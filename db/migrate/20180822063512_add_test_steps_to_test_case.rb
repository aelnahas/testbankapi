# frozen_string_literal: true

class AddTestStepsToTestCase < ActiveRecord::Migration[5.2]
  def change
    add_reference :test_steps, :test_case, foreign_key: true
  end
end
