# frozen_string_literal: true

class CreateTestSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :test_steps do |t|
      t.integer :type
      t.text :content

      t.timestamps
    end
  end
end
